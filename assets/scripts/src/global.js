(function ($) {

  var exports = {}
  var options = {}

  var evolutionFixed = $('.evolution');
  $(window).scroll(function () {
    if($(this).scrollTop() > 600){
      evolutionFixed.addClass('evolution-fixed')
      $('body').css('padding-top', 70)
    }else{
      evolutionFixed.removeClass('evolution-fixed')
      $('body').css('padding-top', 0)
    }
  })

  var btnOpen =  $('.accordion-title').on('click', function(){
    $(this).toggleClass('is-open')
    })
  var btnOpenCard =  $('.card-open').on('click', function(){
    $(this).toggleClass('card-is-active')
    })
  var btnShowAlternative =  $('.btn-save').on('click', function(){
      var checked = $('#radio-ib').is(':checked')
      if (!checked){
        $('#radio-ib').addClass('clickChange')
        $('#radio-ic').prop('checked', false)
        $('#radio-ia').prop('checked', false)

      }else{
        $('#radio-ib').prop('checked', true)
      }
    })
  
  var init = function() {
    evolutionFixed()
    btnOpen()
    btnOpenCard()
    btnShowAlternative()
  }();
  return exports
})(jQuery)